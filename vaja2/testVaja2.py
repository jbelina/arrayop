import unittest
from myarray import BaseArray
from vaja2 import main as m


class MyTestCase(unittest.TestCase):
    def test_printMatrix(self):
        mat = BaseArray((2, 2), data=(0, 1, -3, 5))
        a = m.printMatrix(mat)
        b = "{:^10} {:^10} \n{:^10} {:^10} \n".format(0, 1, -3, 5)
        self.assertMultiLineEqual(a, b, "Matrixs aren't equal - print")

        mat3D = BaseArray((3, 3), dtype=float, data=(1., 2.6, 3., 4., 5., 6.20, 7., 8.3, -69.))
        a2 = m.printMatrix(mat3D)
        b2 = "{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n".format(1.0, 2.6, 3.0, 4.0, 5.0, 6.20, 7.0, 8.3, -69.0)
        self.assertMultiLineEqual(a2, b2, "Matrixs 2 aren't equal - print")

    def test_sortMatrix(self):
        mat = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        # sortiranje po vrsticah
        a = m.sortMatrix(mat, True)
        b = "{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n".format(0, 1, 1, -2, 0, 4, 0, 1, 3)
        self.assertMultiLineEqual(a, b, "Matrixs aren't equals - sort")

        # sortiranje po stolpcih
        mat1 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        a1 = m.sortMatrix(mat1, False)
        b1 = "{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n".format(0, 0, -2, 1, 1, 0, 4, 3, 1)
        self.assertMultiLineEqual(a1, b1, "Matrixs aren't equal - sort")

        # sortiranje 1D tabele
        mat2 = BaseArray((6, ), dtype=int, data=(1, 5, 6, 0, -2, 3))
        a2 = m.sortMatrix(mat2, True)
        b2 = "-2 0 1 3 5 6 "
        self.assertEqual(a2, b2, "Matrixs aren't equal - sort")

    def test_searchValue(self):
        mat = BaseArray((9,), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a = m.searchValue(mat, 2)
        b = (3, 7, 8)
        self.assertTupleEqual(a, b, "Tuples aren't equal - search")

        mat1 = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a1 = m.searchValue(mat1, 2)
        b1 = ((1, 0), (2, 1), (2, 2))
        self.assertTupleEqual(a1, b1, "Tuples aren't equal - search")

        mat2 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2, 5, 0, 0, 1, 7, 3, 7, 0, 9, 6, 6, 2, 1, 7, 2, 0, 6, 5, 0, 6))
        a2 = m.searchValue(mat2, 6)
        b2 = ((1, 3, 0), (1, 1, 1), (1, 1, 2), (1, 3, 3))
        self.assertTupleEqual(a2, b2, "Tuples aren't equal - search")

        mat3 = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a3 = m.searchValue(mat3, 5)
        b3 = False
        self.assertIs(a3, b3, "Tuples aren't equal - search")

    def testSum(self):
        mat = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        mat1 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        mat2 = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
        a = m.sum([mat, mat1, mat2])
        sa = m.printMatrix(a)
        b = BaseArray((3, 3), dtype=float, data=(0.0, 3.5, 3.33, 12.4, 0.4, -6.466, 3.1, 9.0, 0.0))
        sb = m.printMatrix(b)
        self.assertEqual(a.dtype, b.dtype, "Type is not equal - sum matrix")
        self.assertMultiLineEqual(sa, sb, "Data is not equal - sum matrix")

    def testSub(self):
        mat = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        mat1 = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
        a = m.sub([mat, mat1])
        sa = m.printMatrix(a)
        b = BaseArray((3, 3), dtype=float, data=(0.0, -0.5, -0.33000000000000007, -0.40000000000000036, -0.4,
                                                 0.4660000000000002, -0.10000000000000009, 0.0, 0.0))
        sb = m.printMatrix(b)
        self.assertEqual(a.dtype, b.dtype, "Type is not equal - sub matrix")
        self.assertMultiLineEqual(sa, sb, "Data is not equal - sub matrix")

    def testSkalarMul(self):
        mat = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
        a = m.scalarMul(mat, 2)
        sa = m.printMatrix(a)
        b = BaseArray((3, 3), dtype=float, data=(0.0, 3.0, 2.66, 8.8, 0.8, -4.932, 2.2, 6.0, 0.0))
        sb = m.printMatrix(b)
        self.assertTupleEqual(a.shape, b.shape, "Shape is not equal - multiply scalar")
        self.assertEqual(a.dtype, b.dtype, "Type is not equal - multiply scalar")
        self.assertMultiLineEqual(sa, sb, "Data is not equal - multiply scalar")

        mat1 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2, 5, 0, 0, 1, 7, 3, 7, 0, 9, 6, 6, 2, 1, 7, 2, 0, 6, 5, 0, 6))
        a1 = m.scalarMul(mat1, 2.5)
        sa1 = m.printMatrix(a1)
        b1 = BaseArray((2, 4, 4), dtype=float, data=(2.5, 5.0, 20.0, 12.5, 5.0, 7.5, 22.5, 7.5, 0.0, 2.5, 2.5, 5.0, 12.5, 0.0, 0.0, 2.5, 17.5, 7.5,
                                                     17.5, 0.0, 22.5, 15.0, 15.0, 5.0, 2.5, 17.5, 5.0, 0.0, 15.0, 12.5, 0.0, 15.0))
        sb1 = m.printMatrix(b1)
        self.assertTupleEqual(a1.shape, b1.shape, "Shape is not equal - multiply scalar")
        self.assertEqual(a1.dtype, b1.dtype, "Type is not equal - multiply scalar")
        self.assertMultiLineEqual(sa1, sb1, "Data is not equal - multiply scalar")

    def testSkalarDiv(self):
        mat = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
        a = m.scalarDiv(mat, 2)
        sa = m.printMatrix(a)
        b = BaseArray((3, 3), dtype=float, data=(0.0, 0.75, 0.665, 2.2, 0.2, -1.233, 0.55, 1.5, 0.0))
        sb = m.printMatrix(b)
        self.assertTupleEqual(a.shape, b.shape, "Shape is not equal - multiply scalar")
        self.assertEqual(a.dtype, b.dtype, "Type is not equal - multiply scalar")
        self.assertMultiLineEqual(sa, sb, "Data is not equal - multiply scalar")

        mat1 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2, 5, 0, 0, 1, 7, 3, 7, 0, 9, 6, 6, 2, 1, 7, 2, 0, 6, 5, 0, 6))
        a1 = m.scalarDiv(mat1, 2.5)
        sa1 = m.printMatrix(a1)
        b1 = BaseArray((2, 4, 4), dtype=float, data=(0.4, 0.8, 3.2, 2.0, 0.8, 1.2, 3.6, 1.2, 0.0, 0.4, 0.4, 0.8, 2.0, 0.0, 0.0,
                                                     0.4, 2.8, 1.2, 2.8, 0.0, 3.6, 2.4, 2.4, 0.8, 0.4, 2.8, 0.8, 0.0, 2.4, 2.0, 0.0, 2.4))
        sb1 = m.printMatrix(b1)
        self.assertTupleEqual(a1.shape, b1.shape, "Shape is not equal - multiply scalar")
        self.assertEqual(a1.dtype, b1.dtype, "Type is not equal - multiply scalar")
        self.assertMultiLineEqual(sa1, sb1, "Data is not equal - multiply scalar")

    def testMatrixMul(self):
        mat = BaseArray((4, 3), dtype=int, data=(1, 2, 1, 0, -1, 3, 2, 4, 1, 1, 2, 0))
        mat1 = BaseArray((3, 2), dtype=int, data=(3, 2, 1, 4, 0, 5))
        a = m.mult([mat, mat1])
        sa = m.printMatrix(a)
        b = BaseArray((4, 2), dtype=int, data=(5, 15, -1, 11, 10, 25, 5, 10))
        sb = m.printMatrix(b)
        self.assertTupleEqual(a.shape, b.shape, "Shape is not equal - multiply matrix")
        self.assertEqual(a.dtype, b.dtype, "Type is not equal - multiply matrix")
        self.assertMultiLineEqual(sa, sb, "Data is not equal - multiply matrix")

        mat2 = BaseArray((2, 2), dtype=int, data=(1, 4, 0, 5))
        a1 = m.mult([mat, mat2])
        self.assertFalse(a1, "Shape is ok - multiply matrix")

    def testExp(self):
        mat = BaseArray((3, 3), dtype=int, data=(2, -1, -3, 0, 1, 2, 1, 4, 8))
        a = m.exp(mat, 3)
        sa = m.printMatrix(a)
        b = BaseArray((3, 3), dtype=int, data=(-30, -144, -289, 22, 79, 156, 89, 301, 592))
        sb = m.printMatrix(b)
        self.assertTupleEqual(a.shape, b.shape, "Shape is not equal - exp matrix")
        self.assertEqual(a.dtype, b.dtype, "Type is not equal - exp matrix")
        self.assertMultiLineEqual(sa, sb, "Data is not equal - exp matrix")

    def testLog(self):
        mat = BaseArray((3, 3), dtype=int, data=(1, 2, 3, 4, 5, 6, 7, 8, 9))
        a = m.logarithm(mat)
        sa = m.printMatrix(a)
        b = BaseArray((3, 3), dtype=float, data=(0.0, 0.6931471805599453, 1.0986122886681098, 1.3862943611198906, 1.6094379124341003,
                                                 1.791759469228055, 1.9459101490553132, 2.0794415416798357, 2.1972245773362196))
        sb = m.printMatrix(b)
        self.assertTupleEqual(a.shape, b.shape, "Shape is not equal - exp matrix")
        self.assertEqual(a.dtype, b.dtype, "Type is not equal - exp matrix")
        self.assertMultiLineEqual(sa, sb, "Data is not equal - exp matrix")


if __name__ == '__main__':
    unittest.main()
