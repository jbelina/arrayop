import unittest
from myarray import BaseArray
from vaja2 import main as m
import myarray.basearray as ndarray


class MyTestCase(unittest.TestCase):
    def ___test_sum___(self):
        mat = BaseArray((3, 3), dtype=int, data=(0, 1, 2, 4, 0, -2, 1, 3, 0))
        mat_1 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        mat_2 = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
        mat_a = m.sum([mat, mat_1, mat_2])
        mat_sa = m.printMatrix(mat_a)
        mat_b = BaseArray((3, 3), dtype=float, data=(0.0, 3.5, 4.33, 12.4, 0.4, -6.466,
                                                     3.1, 9.0, 0.0))
        mat_sb = m.printMatrix(mat_b)
        self.assertEqual(mat_a.dtype, mat_b.dtype, "Type is not equal - sum matrix")
        self.assertMultiLineEqual(mat_sa, mat_sb, "Data is not equal - sum matrix")

    def ___test_sub___(self):
        mat = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        mat_1 = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
        mat_a = m.sub([mat, mat_1])
        mat_sa = m.printMatrix(mat_a)
        mat_b = BaseArray((3, 3), dtype=float, data=(0.0, -0.5, -0.33000000000000007,
                                                     -0.40000000000000036, -0.4, 0.4660000000000002,
                                                     -0.10000000000000009, 0.0, 0.0))
        mat_sb = m.printMatrix(mat_b)
        self.assertEqual(mat_a.dtype, mat_b.dtype, "Type is not equal - sub matrix")
        self.assertMultiLineEqual(mat_sa, mat_sb, "Data is not equal - sub matrix")

    def ___test_skalar_mul___(self):
        mat = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
        mat_a = m.scalarMul(mat, 2)
        mat_sa = m.printMatrix(mat_a)
        mat_b = BaseArray((3, 3), dtype=float, data=(0.0, 3.0, 2.66, 8.8, 0.8, -4.932,
                                                     2.2, 6.0, 0.0))
        mat_sb = m.printMatrix(mat_b)
        self.assertTupleEqual(mat_a.shape, mat_b.shape, "Shape is not equal - multiply scalar")
        self.assertEqual(mat_a.dtype, mat_b.dtype, "Type is not equal - multiply scalar")
        self.assertMultiLineEqual(mat_sa, mat_sb, "Data is not equal - multiply scalar")

        mat_1 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2, 5, 0,
                                                      0, 1, 7, 3, 7, 0, 9, 6, 6, 2, 1, 7, 2, 0,
                                                      6, 5, 0, 6))
        mat_a1 = m.scalarMul(mat_1, 2.5)
        mat_sa1 = m.printMatrix(mat_a1)
        mat_b1 = BaseArray((2, 4, 4), dtype=float, data=(2.5, 5.0, 20.0, 12.5, 5.0, 7.5,
                                                         22.5, 7.5, 0.0, 2.5, 2.5, 5.0, 12.5,
                                                         0.0, 0.0, 2.5, 17.5, 7.5, 17.5, 0.0,
                                                         22.5, 15.0, 15.0, 5.0, 2.5, 17.5,
                                                         5.0, 0.0, 15.0, 12.5, 0.0, 15.0))
        mat_sb1 = m.printMatrix(mat_b1)
        self.assertTupleEqual(mat_a1.shape, mat_b1.shape, "Shape is not equal - multiply scalar")
        self.assertEqual(mat_a1.dtype, mat_b1.dtype, "Type is not equal - multiply scalar")
        self.assertMultiLineEqual(mat_sa1, mat_sb1, "Data is not equal - multiply scalar")

    def ___test_skalar_div___(self):
        mat = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
        mat_a = m.scalarDiv(mat, 2)
        mat_sa = m.printMatrix(mat_a)
        mat_b = BaseArray((3, 3), dtype=float, data=(0.0, 0.75, 0.665, 2.2, 0.2,
                                                     -1.233, 0.55, 1.5, 0.0))
        mat_sb = m.printMatrix(mat_b)
        self.assertTupleEqual(mat_a.shape, mat_b.shape, "Shape is not equal - multiply scalar")
        self.assertEqual(mat_a.dtype, mat_b.dtype, "Type is not equal - multiply scalar")
        self.assertMultiLineEqual(mat_sa, mat_sb, "Data is not equal - multiply scalar")

        mat_1 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2,
                                                      5, 0, 0, 1, 7, 3, 7, 0, 9, 6, 6, 2,
                                                      1, 7, 2, 0, 6, 5, 0, 6))
        mat_a1 = m.scalarDiv(mat_1, 2.5)
        mat_sa1 = m.printMatrix(mat_a1)
        mat_b1 = BaseArray((2, 4, 4), dtype=float, data=(0.4, 0.8, 3.2, 2.0, 0.8, 1.2, 3.6,
                                                         1.2, 0.0, 0.4, 0.4, 0.8, 2.0, 0.0,
                                                         0.0, 0.4, 2.8, 1.2, 2.8, 0.0, 3.6,
                                                         2.4, 2.4, 0.8, 0.4, 2.8, 0.8, 0.0,
                                                         2.4, 2.0, 0.0, 2.4))
        mat_sb1 = m.printMatrix(mat_b1)
        self.assertTupleEqual(mat_a1.shape, mat_b1.shape, "Shape is not equal - multiply scalar")
        self.assertEqual(mat_a1.dtype, mat_b1.dtype, "Type is not equal - multiply scalar")
        self.assertMultiLineEqual(mat_sa1, mat_sb1, "Data is not equal - multiply scalar")

    def ___test_matrix_mul___(self):
        mat = BaseArray((4, 3), dtype=int, data=(1, 2, 1, 0, -1, 3, 2, 4, 1, 1, 2, 0))
        mat_1 = BaseArray((3, 2), dtype=int, data=(3, 2, 1, 4, 0, 5))
        mat_a = m.mult([mat, mat_1])
        mat_sa = m.printMatrix(mat_a)
        mat_b = BaseArray((4, 2), dtype=int, data=(5, 15, -1, 11, 10, 25, 5, 10))
        mat_sb = m.printMatrix(mat_b)
        self.assertTupleEqual(mat_a.shape, mat_b.shape, "Shape is not equal - multiply matrix")
        self.assertEqual(mat_a.dtype, mat_b.dtype, "Type is not equal - multiply matrix")
        self.assertMultiLineEqual(mat_sa, mat_sb, "Data is not equal - multiply matrix")

        mat_2 = BaseArray((2, 2), dtype=int, data=(1, 4, 0, 5))
        mat_a1 = m.mult([mat, mat_2])
        self.assertFalse(mat_a1, "Shape is ok - multiply matrix")

    def ___test_exp___(self):
        mat = BaseArray((3, 3), dtype=int, data=(2, -1, -3, 0, 1, 2, 1, 4, 8))
        mat_a = m.exp(mat, 3)
        mat_sa = m.printMatrix(mat_a)
        mat_b = BaseArray((3, 3), dtype=int, data=(-30, -144, -289, 22, 79, 156, 89, 301, 592))
        mat_sb = m.printMatrix(mat_b)
        self.assertTupleEqual(mat_a.shape, mat_b.shape, "Shape is not equal - exp matrix")
        self.assertEqual(mat_a.dtype, mat_b.dtype, "Type is not equal - exp matrix")
        self.assertMultiLineEqual(mat_sa, mat_sb, "Data is not equal - exp matrix")

    def ___test_log___(self):
        mat = BaseArray((3, 3), dtype=int, data=(1, 2, 3, 4, 5, 6, 7, 8, 9))
        mat_a = m.logarithm(mat)
        mat_sa = m.printMatrix(mat_a)
        mat_b = BaseArray((3, 3), dtype=float, data=(0.0, 0.6931471805599453, 1.0986122886681098,
                                                     1.3862943611198906, 1.6094379124341003,
                                                     1.791759469228055, 1.9459101490553132,
                                                     2.0794415416798357, 2.1972245773362196))
        mat_sb = m.printMatrix(mat_b)
        self.assertTupleEqual(mat_a.shape, mat_b.shape, "Shape is not equal - exp matrix")
        self.assertEqual(mat_a.dtype, mat_b.dtype, "Type is not equal - exp matrix")
        self.assertMultiLineEqual(mat_sa, mat_sb, "Data is not equal - exp matrix")

    def ___test_All___(self):
            tests = [
                '___test_sum___',
                '___test_sub___',
                '___test_skalar_mul___',
                '___test_skalar_div___',
                '___test_matrix_mul___',
                '___test_exp___',
                '___test_log___'
            ]
            t = unittest.TestSuite()
            t.addTests(map(MyTestCase, tests))
            return t



if __name__ == '__main__':
    unittest.main()
