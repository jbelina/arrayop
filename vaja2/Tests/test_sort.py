import unittest
from myarray import BaseArray
import myarray.basearray as ndarray
from vaja2 import main as m

class MyTestCase(unittest.TestCase):
    def test_sort_matrix(self):
        mat = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        # sortiranje po vrsticah
        mat_a = m.sortMatrix(mat, True)
        mat_b = "{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n"\
            .format(0, 1, 1, -2, 0, 4, 0, 1, 3)
        self.assertMultiLineEqual(mat_a, mat_b, "Matrixs aren't equals - sort")

        # sortiranje po stolpcih
        mat_1 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
        a_1 = m.sortMatrix(mat_1, False)
        b_1 = "{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n"\
            .format(0, 0, -2, 1, 1, 0, 4, 3, 1)
        self.assertMultiLineEqual(a_1, b_1, "Matrixs aren't equal - sort")

        # sortiranje 1D tabele
        mat_2 = BaseArray((6, ), dtype=int, data=(1, 5, 6, 0, -2, 3))
        a_2 = m.sortMatrix(mat_2, True)
        b_2 = "-2 0 1 3 5 6 "
        self.assertEqual(a_2, b_2, "Matrixs aren't equal - sort")


if __name__ == '__main__':
    unittest.main()
