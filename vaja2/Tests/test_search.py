import unittest
from myarray import BaseArray
import myarray.basearray as ndarray
from vaja2 import main as m

class MyTestCase(unittest.TestCase):
    def test_search_value(self):
        mat = BaseArray((9,), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        mat_a = m.searchValue(mat, 2)
        mat_b = (3, 7, 8)
        self.assertTupleEqual(mat_a, mat_b, "Tuples aren't equal - search")

        mat_1 = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a_1 = m.searchValue(mat_1, 2)
        b_1 = ((1, 0), (2, 1), (2, 2))
        self.assertTupleEqual(a_1, b_1, "Tuples aren't equal - search")

        mat_2 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2, 5,
                                                      0, 0, 1, 7, 3, 7, 0, 9, 6, 6, 2, 1, 7,
                                                      2, 0, 6, 5, 0, 6))
        a_2 = m.searchValue(mat_2, 6)
        b_2 = ((1, 3, 0), (1, 1, 1), (1, 1, 2), (1, 3, 3))
        self.assertTupleEqual(a_2, b_2, "Tuples aren't equal - search")

        mat_3 = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
        a_3 = m.searchValue(mat_3, 5)
        b_3 = False
        self.assertIs(a_3, b_3, "Tuples aren't equal - search")


if __name__ == '__main__':
    unittest.main()
