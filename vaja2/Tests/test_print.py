import unittest
from myarray.basearray import BaseArray
import myarray.basearray as ndarray
from vaja2 import main as m
import sys
from importlib.machinery import SourceFileLoader
from pathlib import Path
d = Path(__file__).parents[0]
p = Path(__file__).parents[1]
print(d)
print(p)

#from myarray import BaseArray

#sys.path.insert(0,'arrayop/myarray/basearray')
#sys.path.insert(0,'vaja2/main')
#sys.path.insert(0,'/vaja2/main')
#sys.path.insert(0,'arrayop/vaja2/main')
#sys.path.insert(0,'/arrayop/vaja2/main')

#import basearray
#from myarray.basearray import BaseArray
#from .myarray.basearray import BaseArray
#from .basearray import BaseArray
#from . import BaseArray
#import myarray.basearray as ndarray
#import main as m

#sys.path.append('/vaja2/main')
#from main import *
#import main as m

class MyTestCase(unittest.TestCase):
    def test_print_matrix(self):
        mat = BaseArray((2, 2), data=(0, 1, -3, 5))
        mat_a = m.printMatrix(mat)
        mat_b = "{:^10} {:^10} \n{:^10} {:^10} \n".format(0, 1, -3, 5)
        self.assertMultiLineEqual(mat_a, mat_b, "Matrixs aren't equal - print")

        mat_3d = BaseArray((3, 3), dtype=float, data=(1., 2.6, 3., 4., 5., 6.20, 7., 8.3, -69.))
        a_2 = m.printMatrix(mat_3d)
        b_2 = "{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n{:^10} {:^10} {:^10} \n"\
            .format(1.0, 2.6, 3.0, 4.0, 5.0, 6.20, 7.0, 8.3, -69.0)
        self.assertMultiLineEqual(a_2, b_2, "Matrixs 2 aren't equal - print")


if __name__ == '__main__':
    unittest.main()
