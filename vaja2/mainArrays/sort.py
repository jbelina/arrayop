from myarray.basearray import BaseArray
from vaja2.mainArrays.print import printMatrix

def sortMatrix(mat, sort: type = bool):   # true-rows, false-columns
    arr = []
    a = mat.shape
    if len(a) == 2:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        if sort:
            for i in range(vrstice):
                k = 0
                arr = []
                for j in range(stolpci):
                    arr.append(mat.__getitem__((i, j)))
                mergeSort(arr)
                for j in range(stolpci):
                    mat.__setitem__((i, j), arr[k])
                    k += 1
        else:
            for i in range(stolpci):
                k = 0
                arr = []
                for j in range(vrstice):
                    arr.append(mat.__getitem__((j, i)))
                mergeSort(arr)
                for j in range(vrstice):
                    mat.__setitem__((j, i), arr[k])
                    k += 1
        return printMatrix(mat)
    elif len(a) == 1:
        stolpci = a.__getitem__(0)
        for i in range(stolpci):
            arr.append(mat.__getitem__(i))      # to array for merge sort
        mergeSort(arr)
        j = 0
        for i in arr:
            mat.__setitem__(j, i)              # back to BaseArray
            j += 1
        return printMatrix(mat)


# merge sort
def mergeSort(mat):
    if len(mat) > 1:
        m = len(mat) // 2           # set whole int
        l = mat[:m]
        r = mat[m:]

        mergeSort(l)
        mergeSort(r)

        i = 0
        j = 0
        k = 0
        while i < len(l) and j < len(r):
            if l[i] < r[j]:
                mat[k] = l[i]
                i += 1
            else:
                mat[k] = r[j]
                j += 1
            k += 1

        while i < len(l):
            mat[k] = l[i]
            i += 1
            k += 1

        while j < len(r):
            mat[k] = r[j]
            j += 1
            k += 1


matrika2 = BaseArray((6, ), dtype=int, data=(1, 5, 6, 0, -2, 3))
matrika3 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
matrika4 = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))

print("Sortiranje po vrsticah")
sortMatrix(matrika3, True)
print("\n\nSortiranje po stolpcih")
sortMatrix(matrika4, False)
print("\n\nSortiranje 2D")
sortMatrix(matrika2, False)
