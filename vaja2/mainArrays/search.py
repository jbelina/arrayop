from myarray.basearray import BaseArray

def searchValue(mat, val):
    pos = ()
    if not mat.__contains__(val):
        print("Value", val, "is not found.")
        return False
    a = mat.shape
    count = 0
    if len(a) == 2:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        for i in range(vrstice):
            for j in range(stolpci):
                if mat.__getitem__((i, j)) == val:
                    pos = pos + ((i, j),)
                    count += 1
    elif len(a) == 1:
        stolpci = a.__getitem__(0)
        for i in range(stolpci):
            if mat.__getitem__(i) == val:
                pos = pos + (i, )
                count += 1
    elif len(a) == 3:       # [vrstice, stolpci, diag]
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        diag = a.__getitem__(2)
        for k in range(diag):
            for i in range(vrstice):
                for j in range(stolpci):
                    if mat.__getitem__((i, j, k)) == val:
                        pos = pos + ((i, j, k),)
                        count += 1
    print("Value", val, "is counted", count, "times. Positions:")
    for i in pos:
        print(i, end=" ")

    return pos


matrika5 = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
matrika6 = BaseArray((9, ), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
matrika7 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2, 5, 0, 0, 1,
                                                 7, 3, 7, 0, 9, 6, 6, 2, 1, 7, 2, 0, 6, 5, 0, 6))

print("\n\nIskanje v 2D")
searchValue(matrika5, 8)
print("\n\nIskanje v 2D")
searchValue(matrika5, 2)
print("\n\nIskanje v 1D")
searchValue(matrika6, 2)
print("\n\nIskanje v 3D")
searchValue(matrika7, 6)
