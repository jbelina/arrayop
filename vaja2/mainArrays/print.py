from myarray.basearray import BaseArray

def printMatrix(mat):
    a = mat.shape
    s = ""
    if len(a) == 2:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        for i in range(vrstice):
            for j in range(stolpci):
                print("{:^10}".format(mat.__getitem__((i, j))), end=" ")  # izpise na sredino stolpca
                s += "{:^10} ".format(mat[i, j])
            print()  # new line
            s += "\n"
    elif len(a) == 1:
        for i in mat:
            print(i, end=" ")
            s += "{} ".format(i)
    elif len(a) == 3:       # [diag, vrstice, stolpci]
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        diag = a.__getitem__(2)
        for k in range(diag):
            print(k, "--")
            s += str(k)
            s += "--"
            for i in range(vrstice):
                for j in range(stolpci):
                    print("{:^10}".format(mat.__getitem__((i, j, k))), end=" ")  # izpise na sredino stolpca
                    s += "{:^10} ".format(mat[i, j, k])
                print()  # new line
                s += "\n"
    return s


matrika_3 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
matrika_7 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2,
                                                  5, 0, 0, 1, 7, 3, 7, 0, 9, 6, 6, 2,
                                                  1, 7, 2, 0, 6, 5, 0, 6))

print("Izpis 2D matrike")
printMatrix(matrika_3)
print("Izpis 3D matrike")
printMatrix(matrika_7)
