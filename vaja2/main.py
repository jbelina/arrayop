from myarray.basearray import BaseArray
from math import log


# izpis
def printMatrix(mat):
    a = mat.shape
    s = ""
    if len(a) == 2:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        for i in range(vrstice):
            for j in range(stolpci):
                print("{:^10}".format(mat.__getitem__((i, j))), end=" ")  # izpise na sredino stolpca
                s += "{:^10} ".format(mat[i, j])
            print()  # new line
            s += "\n"
    elif len(a) == 1:
        for i in mat:
            print(i, end=" ")
            s += "{} ".format(i)
    elif len(a) == 3:       # [diag, vrstice, stolpci]
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        diag = a.__getitem__(2)
        for k in range(diag):
            print(k, "--")
            s += str(k)
            s += "--"
            for i in range(vrstice):
                for j in range(stolpci):
                    print("{:^10}".format(mat.__getitem__((i, j, k))), end=" ")  # izpise na sredino stolpca
                    s += "{:^10} ".format(mat[i, j, k])
                print()  # new line
                s += "\n"
    return s


# sortiranje
def sortMatrix(mat, sort: type=bool):   # true-rows, false-columns
    arr = []
    a = mat.shape
    if len(a) == 2:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        if sort:
            for i in range(vrstice):
                k = 0
                arr = []
                for j in range(stolpci):
                    arr.append(mat.__getitem__((i, j)))
                mergeSort(arr)
                for j in range(stolpci):
                    mat.__setitem__((i, j), arr[k])
                    k += 1
        else:
            for i in range(stolpci):
                k = 0
                arr = []
                for j in range(vrstice):
                    arr.append(mat.__getitem__((j, i)))
                mergeSort(arr)
                for j in range(vrstice):
                    mat.__setitem__((j, i), arr[k])
                    k += 1
        return printMatrix(mat)
    elif len(a) == 1:
        stolpci = a.__getitem__(0)
        for i in range(stolpci):
            arr.append(mat.__getitem__(i))      # to array for merge sort
        mergeSort(arr)
        j = 0
        for i in arr:
            mat.__setitem__(j, i)              # back to BaseArray
            j += 1
        return printMatrix(mat)


# merge sort
def mergeSort(mat):
    if len(mat) > 1:
        m = len(mat) // 2           # set whole int
        l = mat[:m]
        r = mat[m:]

        mergeSort(l)
        mergeSort(r)

        i = 0
        j = 0
        k = 0
        while i < len(l) and j < len(r):
            if l[i] < r[j]:
                mat[k] = l[i]
                i += 1
            else:
                mat[k] = r[j]
                j += 1
            k += 1

        while i < len(l):
            mat[k] = l[i]
            i += 1
            k += 1

        while j < len(r):
            mat[k] = r[j]
            j += 1
            k += 1


# Iskanje vrednosti
def searchValue(mat, val):
    pos = ()
    if not mat.__contains__(val):
        print("Value", val, "is not found.")
        return False
    a = mat.shape
    count = 0
    if len(a) == 2:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        for i in range(vrstice):
            for j in range(stolpci):
                if mat.__getitem__((i, j)) == val:
                    pos = pos + ((i, j),)
                    count += 1
    elif len(a) == 1:
        stolpci = a.__getitem__(0)
        for i in range(stolpci):
            if mat.__getitem__(i) == val:
                pos = pos + (i, )
                count += 1
    elif len(a) == 3:       # [vrstice, stolpci, diag]
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        diag = a.__getitem__(2)
        for k in range(diag):
            for i in range(vrstice):
                for j in range(stolpci):
                    if mat.__getitem__((i, j, k)) == val:
                        pos = pos + ((i, j, k),)
                        count += 1
    print("Value", val, "is counted", count, "times. Positions:")
    for i in pos:
        print(i, end=" ")

    return pos


matrika = BaseArray((2, 3), dtype=float, data=(-3.55, -1, 10.53, 2, 1, 2.0102))
matrika2 = BaseArray((6, ), dtype=int, data=(1, 5, 6, 0, -2, 3))
matrika3 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
matrika4 = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
matrika5 = BaseArray((3, 3), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
matrika6 = BaseArray((9, ), dtype=int, data=(0, 0, 0, 2, 0, 0, 0, 2, 2))
matrika7 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2, 5, 0, 0, 1,
                                                 7, 3, 7, 0, 9, 6, 6, 2, 1, 7, 2, 0, 6, 5, 0, 6))


print("Izpis 2D matrike")
printMatrix(matrika3)
print("Izpis 3D matrike")
printMatrix(matrika7)
print("\n\nSortiranje po vrsticah")
sortMatrix(matrika3, True)
print("\n\nSortiranje po stolpcih")
sortMatrix(matrika4, False)
print("\n\nSortiranje 2D")
sortMatrix(matrika2, False)
print("\n\nIskanje v 2D")
searchValue(matrika5, 8)
print("\n\nIskanje v 2D")
searchValue(matrika5, 2)
print("\n\nIskanje v 1D")
searchValue(matrika6, 2)
print("\n\nIskanje v 3D")
searchValue(matrika7, 6)


""" Osnovne Matematične operacije """


# Sestevanje
def sum(mat: BaseArray):
    l = len(mat)
    isSum = False
    if l < 2:
        print("Premalo argumentov")
        return
    temp = mat[0]
    for i in range(l):
        if i+1 < l:
            if mat.__getitem__(i).shape == mat.__getitem__(i + 1).shape:
                sum = BaseArray(mat.__getitem__(i).shape, mat.__getitem__(i).dtype)
                sum = sumIt2d(temp, mat.__getitem__(i + 1))
                temp = sum
                mat.__setitem__(i + 1, sum)
                isSum = True
            else:
                print("Matriki nista enake velikosti")
                return False
    if isSum:
        printMatrix(sum)
    return sum


def sumIt2d(mat1: BaseArray, mat2: BaseArray):
    if mat1.dtype == int and mat2.dtype == int:
        sumM = BaseArray(mat1.shape, dtype=int)
    else:
        sumM = BaseArray(mat1.shape, dtype=float)
    for i in range(mat1.shape.__getitem__(0)):
        for j in range(mat1.shape.__getitem__(1)):
            sumM.__setitem__((i, j), mat1.__getitem__((i, j)) + mat2.__getitem__((i, j)))
    return sumM

# odstevanje
def sub(mat):
    l = len(mat)
    isSub = False
    if l < 2:
        print("Premalo argumentov")
        return
    temp = mat[0]
    for i in range(l):
        if i+1 < l:
            if mat.__getitem__(i).shape == mat.__getitem__(i + 1).shape:
                sub = BaseArray(mat.__getitem__(i).shape)
                sub = subIt2d(temp, mat.__getitem__(i + 1))
                temp = sub
                mat.__setitem__(i + 1, sub)
                isSub = True
            else:
                print("Matriki nista enake velikosti")
                return False
    if isSub:
        printMatrix(sub)
    return sub


def subIt2d(mat1: BaseArray, mat2: BaseArray):
    matS = mat1.shape
    if mat1.dtype == int and mat2.dtype == int:
        sumM = BaseArray(mat1.shape, dtype=int)
    else:
        sumM = BaseArray(mat1.shape, dtype=float)
    for i in range(mat1.shape.__getitem__(0)):
        for j in range(mat1.shape.__getitem__(1)):
            sumM.__setitem__((i, j), mat1.__getitem__((i, j)) - mat2.__getitem__((i, j)))
    return sumM


# mnozenje s skalarjen
def scalarMul(mat: BaseArray, scal):
    a = mat.shape
    if isinstance(scal, float):
        scMat = BaseArray(mat.shape, dtype=float)
    else:
        scMat = BaseArray(mat.shape, dtype=mat.dtype)
    if len(a) == 2:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        for i in range(vrstice):
            for j in range(stolpci):
                scMat.__setitem__((i, j), scal * mat.__getitem__((i, j)))
    elif len(a) == 3:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        diag = a.__getitem__(2)
        for k in range(diag):
            for i in range(vrstice):
                for j in range(stolpci):
                    scMat.__setitem__((i, j, k), scal * mat.__getitem__((i, j, k)))
    elif len(a) == 1:
        stolpci = a.__getitem__(0)
        for i in range(stolpci):
            scMat.__setitem__(i, scal * mat.__getitem__(i))
    printMatrix(scMat)
    return scMat


# deljenje s skalarjem
def scalarDiv(mat: BaseArray, scal):
    a = mat.shape
    scMat = BaseArray(mat.shape, dtype=float)
    if len(a) == 2:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        for i in range(vrstice):
            for j in range(stolpci):
                scMat.__setitem__((i, j), mat.__getitem__((i, j)) / scal)
    elif len(a) == 3:
        vrstice = a.__getitem__(0)
        stolpci = a.__getitem__(1)
        diag = a.__getitem__(2)
        for k in range(diag):
            for i in range(vrstice):
                for j in range(stolpci):
                    scMat.__setitem__((i, j, k), mat.__getitem__((i, j, k)) / scal)
    elif len(a) == 1:
        stolpci = a.__getitem__(0)
        for i in range(stolpci):
            scMat.__setitem__(i, mat.__getitem__(i) / scal)
    printMatrix(scMat)
    return scMat

# Mnozenje matrik
def mult(mat):
    l = len(mat)
    isMul = False
    if l < 2:
        print("Premalo argumentov")
        return False
    temp = mat[0]
    for i in range(l):
        if i+1 < l:
            mat1 = mat.__getitem__(i).shape
            mat2 = mat.__getitem__(i + 1).shape
            if mat1.__getitem__(1) == mat2.__getitem__(0):      # preveri velikost ustreza pravilom mnozenja matrik
                mulMat = BaseArray((mat1.__getitem__(0), mat2.__getitem__(1)))
                mulMat = multiply(temp, mat.__getitem__(i + 1))
                temp = mulMat
                mat.__setitem__((i + 1), mulMat)
                isMul = True
            else:
                print("Velikosti matrik ne ustrezata pravilom")
                return False
    if isMul:
        printMatrix(mulMat)
        return mulMat


def multiply(mat1: BaseArray, mat2: BaseArray):
    a = mat1.shape
    b = mat2.shape
    if mat1.dtype == int and mat2.dtype == int:
        mul = BaseArray((a.__getitem__(0), b.__getitem__(1)), dtype=int)
    else:
        mul = BaseArray((a.__getitem__(0), b.__getitem__(1)), dtype=float)
    for i in range(a.__getitem__(0)):           # vrstice mat1
        for j in range(b.__getitem__(1)):       # stolpci mat2
            for k in range(b.__getitem__(0)):   # vrstice mat2
                mul.__setitem__((i, j), mul.__getitem__((i, j)) + (mat1.__getitem__((i, k)) * mat2.__getitem__((k, j))))
    return mul


# potenciranje
def exp(mat: BaseArray, val):
    a = mat.shape
    if a.__getitem__(0) != a.__getitem__(1):
        print("Matrika ni kvadratna")
        return
    expM = BaseArray((a))
    temp = BaseArray((a))
    temp = mat
    for i in range(val-1):
        expM = multiply(temp, mat)
        temp = expM
    printMatrix(expM)
    return expM


# logaritem
def logarithm(mat: BaseArray):
    a = mat.shape
    logM = BaseArray(a, dtype=float)
    for i in range(a.__getitem__(0)):
        for j in range(a.__getitem__(1)):
            logM.__setitem__((i, j), log(mat.__getitem__((i, j))))
    printMatrix(logM)
    return logM


matrika22 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
matrika33 = BaseArray((3, 3), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
matrika44 = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
matrika333 = BaseArray((9, ), dtype=int, data=(0, 1, 1, 4, 0, -2, 1, 3, 0))
matrika444 = BaseArray((3, 3), dtype=float, data=(0., 1.5, 1.33, 4.4, 0.4, -2.466, 1.1, 3., 0.))
matrika555 = BaseArray((2, 4, 4), dtype=int, data=(1, 2, 8, 5, 2, 3, 9, 3, 0, 1, 1, 2, 5, 0, 0, 1,
                                                   7, 3, 7, 0, 9, 6, 6, 2, 1, 7, 2, 0, 6, 5, 0, 6))
matrika66 = BaseArray((4, 3), dtype=int, data=(1, 2, 1, 0, -1, 3, 2, 4, 1, 1, 2, 0))
matrika77 = BaseArray((3, 2), dtype=int, data=(3, 2, 1, 4, 0, 5))
matrika88 = BaseArray((2, 2), dtype=int, data=(1, 4, 0, 5))
matrika99 = BaseArray((3, 3), dtype=int, data=(2, -1, 1, 0, 1, 2, 1, 0, 1))
matrika10 = BaseArray((3, 3), dtype=int, data=(1, 1, 3, 5, 2, 6, -2, -1, -3))
matrika11 = BaseArray((3, 3), dtype=int, data=(1, 2, 3, 4, 5, 6, 7, 8, 9))


print("\n\n\nRAČUNSKE OPERACIJE\n")
print("SUM:")
sum([matrika33, matrika44, matrika22])
print("\nSUB:")
sub([matrika33, matrika44])
print("\nMnozenje s skalarjem 1D")
scalarMul(matrika333, -2)
print("\n\nMnozenje s skalarjem 2D")
scalarMul(matrika444, 2)
print("\nMnozenje s skalarjem 3D")
scalarMul(matrika555, 2)
print("\nDeljenje s skalarjem 1D")
scalarDiv(matrika333, -2)
print("\n\nDeljenje s skalarjem 2D")
scalarDiv(matrika444, 2)
print("\nDeljenje s skalarjem 3D")
scalarDiv(matrika555, 2)
print("\nMnozenje dveh matrik")
mult([matrika66, matrika77])
print("\nMnozenje dveh matrik (nepravilne vel)")
mult([matrika66, matrika88])
print("\nPotenciranje matrik")
exp(matrika10, 3)
print("\nPotenciranje matrik")
exp(matrika99, 3)
print("\nLogaritmiranje matrik")
logarithm(matrika11)
print("\n\n\n")

matrika11111 = BaseArray((3, 3), dtype=float, data=(0, 1, 1, 4, 0, 2, 1, 3, 0))
matrika22222 = BaseArray((3, 3), dtype=float, data=(1., 1., 1., 4., 1., 2., 1., 3., 1.))
matrika33333 = logarithm(matrika22222)
print("\ntype:", matrika33333.dtype)
print("\n\n\n")
a = matrika22222.__iter__()
print(a, "\n\n")
